# Recipy

## Prerequistes

- Install docker
- Create `.env` file in `arch` folder
- Install deps: `bin/npm install`

## Dev dev
```
bin/server
```

## Use npm
```
bin/npm
```

## Use angular cli
```
bin/ng
```
